<% local view, viewlibrary, page_info, session = ... %>
<% html = require("acf.html") %>

<script type="text/javascript">
	if (typeof jQuery == 'undefined') {
		document.write('<script type="text/javascript" src="<%= html.html_escape(page_info.wwwprefix) %>/js/jquery-latest.js"><\/script>');
	}
</script>

<script type="text/javascript">
	if (typeof $.tablesorter == 'undefined') {
		document.write('<script type="text/javascript" src="<%= html.html_escape(page_info.wwwprefix) %>/js/jquery.tablesorter.js"><\/script>');
	}
</script>

<script type="text/javascript">
	$(document).ready(function() {
		$("#list").tablesorter({widgets: ['zebra']});
	});
</script>

<% viewlibrary.dispatch_component("status") %>

<% viewlibrary.dispatch_component("viewconfig") %>

<% local header_level = htmlviewfunctions.displaysectionstart(cfe({label="Status"}), page_info) %>
<% local header_level2 = htmlviewfunctions.displaysectionstart(cfe({label="Connected clients"}), page_info, htmlviewfunctions.incrementheader(header_level)) %>
<table id="list" class="tablesorter"><thead>
	<tr>
		<th>Common Name<br/>Connected at</th>
		<th>Virtual Address</th>
		<th>Real Address</th>
		<th>Bytes<br/>Received</th>
		<th>Bytes<br/>Sent</th>
	</tr>
</thead><tbody>
<% for i in ipairs(view.value) do %>
	<tr>
		<td><%= html.html_escape(view.value[i].CN) %></td>
		<td><%= html.html_escape(view.value[i].VIRTADDR) %></td>
		<td><%= html.html_escape(view.value[i].REALADDR) %></td>
		<td><%= html.html_escape(view.value[i].BYTEsrcV) %></td>
		<td><%= html.html_escape(view.value[i].BYTESSND) %></td>
	</tr>
	<tr>
		<td colspan=5><%= html.html_escape(view.value[i].CONN) %></td>
	</tr>
<% end %>

</tbody></table>
<% htmlviewfunctions.displaysectionend(header_level2) %>
<% htmlviewfunctions.displaysectionend(header_level) %>
