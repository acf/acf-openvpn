local mymodule = {}

mymodule.mvc = {}
mymodule.mvc.on_load = function(self, parent)
	self.model.set_processname(string.match(self.conf.prefix, "[^/]+"))
end

mymodule.default_action = "status"

mymodule.status = function(self)
	return self.model.getstatus()
end

mymodule.startstop = function(self)
	return self.handle_form(self, self.model.get_startstop, self.model.startstop_service, self.clientdata)
end

mymodule.expert = function(self)
	return self.handle_form(self, self.model.get_filecontent, self.model.update_filecontent, self.clientdata, "Save", "Edit Config File", "Config File Saved")
end

mymodule.viewconfig = function(self)
	return self.model.get_config()
end

mymodule.statusinfo = function (self)
	return self.model.getclientinfo()
end

mymodule.logfile = function(self)
	return self.model.get_logfile(self, self.clientdata)
end

function mymodule.listcerts(self)
	return self.model.list_certs()
end

function mymodule.deletecert(self)
	return self.handle_form(self, self.model.get_delete_cert, self.model.delete_cert, self.clientdata, "Delete", "Delete Certificate", "Certificate Deleted")
end

function mymodule.uploadcert (self)
	return self.handle_form(self, self.model.new_upload_cert, self.model.upload_cert, self.clientdata, "Upload", "Upload Certificate", "Certificate Uploaded")
end

function mymodule.viewcert(self)
	return self.model.view_cert(self.clientdata.cert)
end

function mymodule.generatedhparams(self)
	return self.handle_form(self, self.model.get_generate_dh_params, self.model.generate_dh_params, self.clientdata, "Generate", "Generate Diffie Hellman parameters", "Diffie Hellman parameters generated")
end

return mymodule
