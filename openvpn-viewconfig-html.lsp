<% local view, viewlibrary, page_info, session = ... %>
<% format = require("acf.format") %>
<% html = require("acf.html") %>
<% local shortname = string.gsub(view.value.name, "^.*/", "") %>

<% local header_level = htmlviewfunctions.displaysectionstart(cfe({label=format.cap_begin_word(view.value.type).." Config"}), page_info) %>
<% local header_level2 = htmlviewfunctions.displaysectionstart(cfe({label=format.cap_begin_word(view.value.type).." settings"}), page_info, htmlviewfunctions.incrementheader(header_level)) %>

<% htmlviewfunctions.displayitemstart() %>
Mode
<% htmlviewfunctions.displayitemmiddle() %>
<%= html.html_escape(view.value.type) %>
<% htmlviewfunctions.displayitemend() %>

<% htmlviewfunctions.displayitemstart() %>
User device
<% htmlviewfunctions.displayitemmiddle() %>
<%= html.html_escape(view.value.dev) %>
<% htmlviewfunctions.displayitemend() %>

<% if view.value.type == "server" then %>
<% htmlviewfunctions.displayitemstart() %>
Listens on
<% htmlviewfunctions.displayitemmiddle() %>
<%= html.html_escape(view.value["local"]) %>:<%= html.html_escape(view.value.port) %> (<%= html.html_escape(view.value.proto) %>)
<% htmlviewfunctions.displayitemend() %>
<% end %>

<% if view.value.type == "client" then %>
<% htmlviewfunctions.displayitemstart() %>
Remote server
<% htmlviewfunctions.displayitemmiddle() %>
<% if string.find(view.value.remote, "%s") then io.write(html.html_escape(string.gsub(view.value.remote, "%s+", ":"))) else io.write(html.html_escape(view.value.remote .. (view.value.rport or view.value.port or "1194"))) end %> (<%= html.html_escape(view.value.proto) %>)
<% htmlviewfunctions.displayitemend() %>
<% end %>

<% htmlviewfunctions.displayitemstart() %>
Logfile
<% htmlviewfunctions.displayitemmiddle() %>
<% if ( view.value.log ) then %><%= html.link{value = page_info.script .. page_info.prefix .. page_info.controller .. "/logfile?name=" .. view.value.name, label=view.value.log } %><% else %>Syslog<% end %> (Verbosity level: <%= html.html_escape(view.value.verb) %>)
<% htmlviewfunctions.displayitemend() %>

<% if view.value.type == "server" then %>
<% local header_level3 = htmlviewfunctions.displaysectionstart(cfe({label="Connected clients status"}), page_info, htmlviewfunctions.incrementheader(header_level2)) %>

<% htmlviewfunctions.displayitemstart() %>
Last status was recorded
<% htmlviewfunctions.displayitemmiddle() %>
<%= html.html_escape(view.value.client_lastupdate) %> (This was <b><%= html.html_escape(view.value.client_lastdatechangediff) %></b> ago)
<% htmlviewfunctions.displayitemend() %>

<% htmlviewfunctions.displayitemstart() %>
Maximum clients
<% htmlviewfunctions.displayitemmiddle() %>
<%= html.html_escape(view.value["max-clients"]) %>
<% htmlviewfunctions.displayitemend() %>

<% htmlviewfunctions.displayitemstart() %>
Connected clients
<% htmlviewfunctions.displayitemmiddle() %>
<%= html.html_escape(view.value.client_count) %>
<% htmlviewfunctions.displayitemend() %>

<% htmlviewfunctions.displaysectionend(header_level3) %>
<% end %>


<% if view.value.dh or view.value.ca or view.value.cert or view.value.key or view.value.tls or view.value.crl then %>
<% htmlviewfunctions.displaysectionend(header_level2) %>
<% htmlviewfunctions.displaysectionstart(cfe({label="Certificate files"}), page_info, header_level2) %>

<% if (view.value.dh) then %>
<% htmlviewfunctions.displayitemstart() %>
DH
<% htmlviewfunctions.displayitemmiddle() %>
<%= html.link{value = page_info.script .. page_info.prefix .. page_info.controller .. "/viewcert?cert=" .. view.value.dh, label=view.value.dh } %>
<% htmlviewfunctions.displayitemend() %>
<% end %>

<% if (view.value.ca) then %>
<% htmlviewfunctions.displayitemstart() %>
CA Certificate
<% htmlviewfunctions.displayitemmiddle() %>
<%= html.link{value = page_info.script .. page_info.prefix .. page_info.controller .. "/viewcert?cert=" .. view.value.ca, label=view.value.ca } %>
<% htmlviewfunctions.displayitemend() %>
<% end %>

<% if (view.value.cert) then %>
<% htmlviewfunctions.displayitemstart() %>
Certificate
<% htmlviewfunctions.displayitemmiddle() %>
<%= html.link{value = page_info.script .. page_info.prefix .. page_info.controller .. "/viewcert?cert=" .. view.value.cert, label=view.value.cert } %>
<% htmlviewfunctions.displayitemend() %>
<% end %>

<% if (view.value.key) then %>
<% htmlviewfunctions.displayitemstart() %>
Private Key
<% htmlviewfunctions.displayitemmiddle() %>
<%= html.html_escape(view.value.key) %>
<% htmlviewfunctions.displayitemend() %>
<% end %>

<% if (view.value.tls) then %>
<% htmlviewfunctions.displayitemstart() %>
TLS Authentication
<%= html.html_escape(view.value.tls) %><% -- html.link{value = page_info.script .. page_info.prefix .. page_info.controller .. "/pem_info?name=" .. view.value.tls  , label=view.value.tls } %>
<% htmlviewfunctions.displayitemend() %>
<% end %>

<% if (view.value.crl) then %>
<% htmlviewfunctions.displayitemstart() %>
CRL Verify File
<% htmlviewfunctions.displayitemmiddle() %>
<%= html.html_escape(view.value.crl) %><% -- html.link{value = page_info.script .. page_info.prefix .. page_info.controller .. "/pem_info?name=" .. view.value.crl  , label=view.value.crl } %>
<% htmlviewfunctions.displayitemend() %>
<% end %>

<% end %>

<% htmlviewfunctions.displaysectionend(header_level2) %>
<% htmlviewfunctions.displaysectionend(header_level) %>
