<% local view, viewlibrary, page_info, session = ... %>
<% htmlviewfunctions = require("htmlviewfunctions") %>
<% html = require("acf.html") %>

<% local header_level = htmlviewfunctions.displaysectionstart(cfe({label="Certificate Details"}), page_info) %>
<pre><%= html.html_escape(view.value.value) %></pre>
<% htmlviewfunctions.displaysectionend(header_level) %>
