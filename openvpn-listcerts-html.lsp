<% local view, viewlibrary, page_info, session = ... %>
<% htmlviewfunctions = require("htmlviewfunctions") %>
<% html = require("acf.html") %>

<script type="text/javascript">
	if (typeof jQuery == 'undefined') {
		document.write('<script type="text/javascript" src="<%= html.html_escape(page_info.wwwprefix) %>/js/jquery-latest.js"><\/script>');
	}
</script>

<script type="text/javascript">
	if (typeof $.tablesorter == 'undefined') {
		document.write('<script type="text/javascript" src="<%= html.html_escape(page_info.wwwprefix) %>/js/jquery.tablesorter.js"><\/script>');
	}
</script>

<script type="text/javascript">
	$(document).ready(function() {
		$("#list").tablesorter({headers: {0:{sorter: false}}, widgets: ['zebra']});
	});
</script>

<% htmlviewfunctions.displaycommandresults({"deletecert", "generatedhparams"}, session) %>
<% htmlviewfunctions.displaycommandresults({"uploadcert"}, session, true) %>

<% local header_level = htmlviewfunctions.displaysectionstart(view, page_info) %>
<table id="list" class="tablesorter"><thead>
	<tr>
		<th>Action</th>
		<th>Certificate</th>
	</tr>
</thead><tbody>
<% local cert = cfe({ type="hidden", value="" }) %>
<% local redir = cfe({ type="hidden", value=page_info.orig_action }) %>
<% for i,crt in ipairs(view.value) do %>
	<tr>
		<td>
		<% cert.value = crt %>
		<% htmlviewfunctions.displayitem(cfe({type="form", value={cert=cert}, label="", option="Delete", action="deletecert" }), page_info, -1) %>
		<% if not string.find(crt, "%-key") then %>
		<% htmlviewfunctions.displayitem(cfe({type="link", value={cert=cert, redir=redir}, label="", option="View", action="viewcert"}), page_info, -1) %>
		<% end %>
		</td>
		<td><%= html.html_escape(crt) %></td>
	</tr>
<% end %>
</tbody></table>
<% htmlviewfunctions.displaysectionend(header_level) %>

<% if viewlibrary.dispatch_component and viewlibrary.check_permission("uploadcert") then
	viewlibrary.dispatch_component("uploadcert")
end %>

<% if viewlibrary.check_permission("generatedhparams") then %>
<% htmlviewfunctions.displaysectionstart(cfe({label="Diffie Hellman Parameters"}), page_info, header_level) %>
<% htmlviewfunctions.displayitem(cfe({type="form", value={}, label="Generate Diffie Hellman parameters", option="Generate", action="generatedhparams" }), page_info, 0) %>
<% htmlviewfunctions.displaysectionend(header_level) %>
<% end %>
